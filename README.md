# Jbrain                    
Neural network in javascript

[![Code Climate](https://codeclimate.com/github/abtExp/Jbrain/badges/gpa.svg)](https://codeclimate.com/github/abtExp/Jbrain) [![Build Status](https://travis-ci.org/abtExp/Jbrain.svg?branch=master)](https://travis-ci.org/abtExp/Jbrain)  [![npm version](https://badge.fury.io/js/jbrain.svg)](https://badge.fury.io/js/jbrain)
[![Coverage Status](https://coveralls.io/repos/github/abtExp/Jbrain/badge.svg?branch=master)](https://coveralls.io/github/abtExp/Jbrain?branch=master)

Work in progress.

* Initially a simple neural network implementation( with quadratic cost function,sigmoid activation and stochastic gradient descent).
* Will update further for preventing overfitting(L1,L2 regularisation) , more cost functions(log likelihood, cross entropy etc.),    
  activation functions(softmax).
* Other machine learning algorithms like SVM. 

----------------------------------------------------------------------------------------------------------------------------------------  
--Any help or suggestion is appreciated.
The Ndime.js file in helper directory is another of my projects, Here is the link to it : 
<a href="https://abt10.github.io/Vector_JS">Vector_JS</a> .
