module.exports = function(z_i,z){
    let avg = 0;
    for(let i=0; i<z.length; i++){
        avg += Math.exp(z[i]);
    }
    return (Math.exp(z_i)/avg);
}