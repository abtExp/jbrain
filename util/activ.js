var sigmoid = require('./activ/sigmoid');
var softmax = require('./activ/softmax');

var activ = {
    sigmoid : sigmoid,
    softmax : softmax
}

module.exports = activ;